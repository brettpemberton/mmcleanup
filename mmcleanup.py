#!/usr/bin/python

import logging
import rpm
import os
from optparse import OptionParser

packages = {'mmservices': '/var/www/mmservices/mmservices',
            'mmcommon': '/var/www/html/common',
            'mmmanager': '/var/www/html/manager'}
paths = {'mmservices': '/var/www/mmservices',
         'mmcommon': '/var/www/common',
         'mmmanager': '/var/www/manager'}
exclude = {'mmservices': ['mmservices-mcs'],
           'mmcommon': [],
           'mmmanager': []} 
keep_packages = 3

logger = logging.getLogger('mmcleanup')
logger.setLevel(logging.INFO)
ch = logging.StreamHandler()
logger.addHandler(ch)


def get_latest_version(package):
    if not packages[package]:
        return False
    if not os.path.islink(packages[package]):
        logger.error("No symlink found in %s, Can't be sure what version is production" % (packages[package]))
        return False
    return os.path.split(os.path.realpath(os.readlink(packages[package])))[1]


def get_installed_versions(package):
    installed_versions = []
    ts = rpm.TransactionSet()
    master_list = ts.dbMatch()
    for h in master_list:
        if h['name'].startswith(package):
            logger.debug("Found a match for %s in installed package - %s-%s-%s" % (package, h['name'], h['version'], h['release']))
            installed_versions.append("%s-%s-%s" % (h['name'], h['version'], h['release']))
    return sorted(installed_versions)


def check_installs(package):
    to_rm = []
    if not os.path.isdir(paths[package]):
        return []
    ts = rpm.TransactionSet()
    base_dir = paths[package]
    for item in os.listdir(base_dir):
        if os.path.isdir(os.path.join(base_dir, item)) and not os.path.islink(os.path.join(base_dir, item)) and not item in exclude[package]:
            # check if this is installed via rpm
            if ts.dbMatch('basenames', os.path.join(base_dir, item)).count() != 1:
                logger.debug("Found a manual install in %s" % (os.path.join(base_dir, item)))
                to_rm.append(os.path.join(base_dir, item))
    return to_rm


if __name__ == "__main__":

    usage = "usage: %prog [options] package"
    parser = OptionParser(usage)
    parser.add_option("-p", "--perform", dest="perform", action="store_true",
                            help="Perform operation (Dry Run by default)")
    parser.add_option("-v", "--verbose", dest="verbose", action="store_true",
                            help="enable verbose output")

    (options, args) = parser.parse_args()

    if options.perform:
        perform = options.perform
    else:
        perform = False

    if options.verbose:
        logger.setLevel(logging.DEBUG)

    for package in packages:
        logger.debug("Checking %s" % (package))
        latest = get_latest_version(package)
        logger.debug("Latest version: %s" % (latest))
        check = check_installs(package)
        logger.debug("Check returned: %s" % (check))
        if latest and latest in check:
            logger.error("We're trying to remove a manual install which is the most recent version")
            sys.exit(1)
        if len(check) > 0:
            rm = "rm -rf %s" % " ".join(check)
            logger.info(rm)
        versions = get_installed_versions(package)
        if len(versions) > keep_packages:
            logger.debug("We should keep these versions: ")
            logger.debug(versions[-keep_packages:])
            logger.debug("We should remove these versions: ")
            logger.debug(versions[:-keep_packages])
            if latest and latest in versions[:-keep_packages]:
                logger.error("We're trying to remove an RPM which is the most recent version")
                sys.exit(1)
            # Do an rpm verify to check the ones we're keeping are OK
            erase = "rpm -e %s" % (" ".join(versions[:-keep_packages]))
            logger.info(erase)
